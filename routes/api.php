<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Login to Logout
Route::post('/login',"ApiController@Login");
Route::post('/signup',"ApiController@Register");
Route::middleware('auth:api')->post('/logout',"ApiController@Logout");
Route::middleware('auth:api')->resource('/courses',"CourseController");
Route::middleware('auth:api')->get('/course/{slug}',"CourseController@Access");
Route::middleware('auth:api')->resource('/coupons',"CouponController");
Route::middleware('auth:api')->resource('/purchases',"PurchaseController");
Route::resource('/purchases',"PurchaseController");
Route::middleware('auth:api')->resource('/video',"ResourceUsageController");
Route::any('/jcalls',"ApiController@Request");
Route::post('/verify-email',"ApiController@EmailVerify");

Route::middleware('auth:api')->get('/videos',"ApiController@Videos");
Route::middleware('auth:api')->get('/account',"ApiController@Account");
Route::middleware('auth:api')->get('/payment/{id}',"ApiController@Transaction");
Route::middleware('auth:api')->get('/payment',"ApiController@Payment");


Route::middleware('auth:api')->get('/user', function (Request $request) {
    $user = $request->user();
    $user->courses = $user->Courses;
    foreach ($user->courses as $key) {
        $key->course = $key->Course;
    }
    return $user;
});
Route::get('/pap', function (Request $request) {
    
});

Route::middleware('auth:api')->get('/user/{email}','ApiController@user');
Route::middleware('auth:api')->put('/user/{id}','ApiController@UpdateUser');


Route::get('/admin/courses','CourseController@courses');
Route::get('/admin/users','ApiController@Users');
