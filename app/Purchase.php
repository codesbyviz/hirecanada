<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    public function User()
    {
        return $this->belongsTo("App\User","user","id");
    }
    public function Course()
    {
        return $this->belongsTo("App\Course","course","id");
    }
}
