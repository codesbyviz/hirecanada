<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function Videos()
    {
        return $this->hasMany("App\Video","course","id");
    }
    public function Purchases()
    {
        return $this->hasMany("App\Purchase","course","id");
    }
}
