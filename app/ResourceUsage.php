<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourceUsage extends Model
{
    public function Resource()
    {
        return $this->hasOne("App\Video","id","resource");
    }
}
