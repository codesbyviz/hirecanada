<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->active){
            $active = Coupon::where("status","0")->get();
        }
        else{
            $active = Coupon::orderBy('created_at','desc')->get();
        }
        return $active;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new = [];
        for ($i=0; $i < $request->coupons_number; $i++) { 
            $new[$i] = new Coupon;
            $new[$i]->coupon = strtoupper(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 6));
            $new[$i]->discount = json_encode(['type'=>$request->type,'value'=>$request->value]);
            $new[$i]->status = 1;
            $new[$i]->usedby = null;
            $new[$i]->save();
        }
        return response($new,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show($code)
    {
        $coupon = Coupon::where('coupon',$code)->first();
        return $coupon;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        //
    }
}
