<?php

namespace App\Http\Controllers;

use App\Purchase;
use App\ResourceUsage;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->formdata;
        $purchase = new Purchase;
        $purchase->course = $data['course'];
        $purchase->user = $data['user'];
        $purchase->status = $data['status'];
        $purchase->transaction = json_encode($data['transaction']);
        $purchase->save();
        
        // if($data['transaction'])

        return response()->json("Success",200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase $purchase)
    {
        $purchase->course = $purchase->Course;
        $purchase->user = $purchase->User;
        $purchase->videos = $purchase->Course->Videos;
        foreach ($purchase->videos as $key) {
            $key->usage = ResourceUsage::where(['user'=>$purchase->user->id,'resource'=>$key->id])->first();
        }
        return $purchase;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        //
    }
}
