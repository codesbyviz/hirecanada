<?php

namespace App\Http\Controllers;

use App\ResourceUsage;
use App\Video;
use Illuminate\Http\Request;

class ResourceUsageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->id();
        $resource = ResourceUsage::where("resource",$request->video)->where('user',$user)->first();
        return $resource;
        if($resource){
            $resource->video_finished = $request->stoppedat; 
            return response()->json("Updated",200);
        }
        else{
            $resource = new ResourceUsage;
            $resource->user = $user; 
            $resource->resource = $request->video; 
            $resource->access = true; 
            $resource->activity_completed = false; 
            $resource->video_finished =$request->stoppedat;
            $resource->save();
            return response()->json("Updated",200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ResourceUsage  $resourceUsage
     * @return \Illuminate\Http\Response
     */
    public function show(ResourceUsage $resourceUsage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ResourceUsage  $resourceUsage
     * @return \Illuminate\Http\Response
     */
    public function edit(ResourceUsage $resourceUsage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ResourceUsage  $resourceUsage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ResourceUsage $resourceUsage)
    {
        $user = auth()->id();
        $resource = ResourceUsage::where("resource",$request->video)->where('user',$user)->first();

        if($resource){
            $resource->video_finished       = ($request->video_finished) ?$request->video_finished:$resource->video_finished; 
            $resource->access               = ($request->access) ?$request->access:$resource->access; 
            $resource->activity_completed   = ($request->activity_completed) ?$request->activity_completed:$resource->activity_completed; 
            if($request->activity_completed){
                $nextResourceItem = Video::where(['order'=>$resource->Resource->order+1,'course'=>$resource->Resource->course])->first();
                if($nextResourceItem){
                    $serea = new ResourceUsage;
                    $serea->user = $user; 
                    $serea->resource = $nextResourceItem->id; 
                    $serea->access = true; 
                    $serea->activity_completed = false; 
                    $serea->video_finished = null;
                    $serea->save();
                }
            }
            $resource->save(); 
            return response()->json($resource,200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ResourceUsage  $resourceUsage
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResourceUsage $resourceUsage)
    {
        //
    }
}
