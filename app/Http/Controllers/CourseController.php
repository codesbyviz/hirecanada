<?php

namespace App\Http\Controllers;

use App\Course;
use App\Purchase;
use App\ResourceUsage;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses    = Course::all();;
        foreach ($courses as $key) {
            $key->videos = $key->Videos;
            $U1500 = $key->Purchases->where('user',auth()->user()->id)->where('status',1)->first();
            if($U1500){
                $key->user = $U1500;
            }
            else{
                $key->user = false;
            }
            // $key->user = "sale";
        }
        return $courses;
    }
    public function courses()
    {
        $courses    = Course::all();;
        return $courses;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $course = new Course;
        $data =  $request->course;
        $course->name = $data['name'];
        $course->slug = $data['slug'];
        $course->description = $data['description'];
        $course->features = $data['features'];
        $course->price = $data['price'];
        $course->gst_percentage = $data['gst_percentage'];
        $course->tags = null;
        $course->status = $data['status'];
        $course->save();
        return response()->json($course,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $course = Course::with(['Videos'])->where('slug',$request->course)->first();
        return $course;
    }

    /**
     * Display the specified resource, if has Access.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function Access(Request $request,$course)
    {
        $course     = Course::where('slug',$course)->first();
        if($course){
            $purchase   = Purchase::with('Course')->where("user",auth()->user()->id)->where("course",$course->id)->first();
            if($purchase){
                if($purchase->expiry && $purchase->expiry <= now()){
                    return response()->json("access expired",412);
                }
                else{
                    $purchase->videos = $course->Videos()->orderBy('order',"asc")->get();
                    foreach ($purchase->videos as $key) {
                        $key->files = json_decode($key->files);
                        $key->resource = ResourceUsage::where('user',auth()->id())->where('resource',$key->id)->first();
                        if($key->order == 1 && empty($key->resource)){
                            $key->resource = new ResourceUsage;
                            $key->resource->user = auth()->id();
                            $key->resource->resource = $key->id;
                            $key->resource->access = "true";
                            $key->resource->video_finished = null;
                            $key->resource->activity_completed = false;
                            $key->resource->save();
                        }
                    }
                    return $purchase;
                }
            }
            else{
                return response()->json("not purchased",402);
            }
        }
        else{
            return response()->json("Invalid Course",400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        $data =  $request->course;
        $course->name = $data['name'];
        $course->description = $data['description'];
        $course->features = $data['features'];
        $course->price = $data['price'];
        $course->gst_percentage = $data['gst_percentage'];
        $course->tags = $data['tags'];
        $course->status = $data['status'];
        $course->save();
        return response()->json($course,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        //
    }
}
