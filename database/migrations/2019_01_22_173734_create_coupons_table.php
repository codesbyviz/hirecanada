<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Coupon;
class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('coupon');
            $table->longtext('discount');
            $table->string('status')->default(1);
            $table->string('usedby')->nullable();
            $table->timestamps();
        });
        $coupon = new Coupon;
        $coupon->coupon = "BX2563";
        $coupon->discount = '{"type": "percentage", "value": "100"}';
        $coupon->save();

        $coupon2 = new Coupon;
        $coupon2->coupon = "PX7256";
        $coupon2->discount = '{"type": "percentage", "value": "30"}';
        $coupon2->save();

        $coupon3 = new Coupon;
        $coupon3->coupon = "XX5564";
        $coupon3->discount = '{"type": "amount", "value": "1500"}';
        $coupon3->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
