<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Course;
class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->longtext('description');
            $table->longtext('features');
            $table->string('price');
            $table->string('gst_percentage');
            $table->string('sale_price')->nullable();
            $table->string('tags')->nullable();
            $table->string('status')->default(1);
            $table->timestamps();
        });

        $course = new Course;
        $course->name = "This Course is meant for some Professionals";
        $course->slug = "for-professionals";
        $course->description = "One of the main goals of Canadian immigration is to welcome skilled newcomers who will contribute to Canada's growing economy. Skilled workers who settle in Canada on a permanent basis are especially valuable to Canada's economy and the strength of its workforce. Successful applicants of the Skilled Worker Immigration programs will receive a Canadian Immigration (permanent resident) Visa, allowing the applicant to immigrate to Canada with his or her family.";
        $course->features = "<ul> <li>First item</li> <li>Second item</li> <li>Third item</li> <li>Fourth item</li> <li>First item</li> <li>Second item</li> <li>Third item</li> <li>Fourth item</li> </ul>";
        $course->price = "10000";
        $course->gst_percentage = "18";
        $course->save();

        $course2 = new Course;
        $course2->name = "This Course is meant for some Students";
        $course2->slug = "for-students";
        $course2->description = "One of the main goals of Canadian immigration is to welcome skilled newcomers who will contribute to Canada's growing economy. Skilled workers who settle in Canada on a permanent basis are especially valuable to Canada's economy and the strength of its workforce. Successful applicants of the Skilled Worker Immigration programs will receive a Canadian Immigration (permanent resident) Visa, allowing the applicant to immigrate to Canada with his or her family.";
        $course2->features = "<ul> <li><strong><a href=\"https://www.canadavisa.com/federal-skilled-worker-program-fswp.html\" rel=\"alternate\" hreflang=\"en\">Federal Skilled Worker</a></strong>: This program is for individuals with certain work experience who intend to reside in any province or territory outside of the province of Quebec.</li> </ul>";
        $course2->price = "9999";
        $course2->gst_percentage = "18";
        $course2->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
