<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_usages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user');
            $table->string('resource');
            $table->string('access')->nullable();
            $table->string('video_finished')->nullable();
            $table->string('activity_completed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_usages');
    }
}
