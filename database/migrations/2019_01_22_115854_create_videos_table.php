<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Video;
class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('course');
            $table->string('name');
            $table->longtext('description')->nullable();
            $table->longtext('endgame')->nullable();
            $table->longtext('files');
            $table->string('order');
            $table->string('status');
            $table->timestamps();
        });
        $videos = new Video;
        $videos->course = 1;
        $videos->order = 1;
        $videos->status = 1;
        $videos->name = "Let's Get Started";
        $videos->description = "It's the start of a new year, and there couldn't be a better time to look ahead at what the next 12 months might hold for video marketers. Here at Wyzowl, we've just published the results of our fourth annual State of Video Marketing Survey, and the numbers suggest this is set to be another big year for video -- among both marketers and consumers. These statistics were gathered by surveying 570 unique respondents in December 2017. The respondents consisted of both marketing professionals and online consumers. Discover videos, templates, tips, and other resources dedicated to helping you launch an effective video marketing strategy. Respondents were categorized as marketers or consumers according to their answer to a preliminary question, to ensure they were only prompted to answer relevant questions.";
        $videos->endgame = "How about some Chat with me? Go on and click the chat icon to get chatting with me ?";
        $videos->files = json_encode(["tracks"=>[["src"=>"/videos/subtitles/english.vtt","kind"=>"captions","label"=>"English","srclang"=>"en",],["src"=>"/videos/subtitles/french.vtt","kind"=>"captions","label"=>"French","srclang"=>"fr",],["src"=>"/videos/subtitles/german.vtt","kind"=>"captions","label"=>"German","srclang"=>"gr",],],"sources"=>[["src"=>"/videos/360.mp4","quality"=>"360"],["src"=>"/videos/720.mp4","quality"=>"720"],],"thumbnail"=>"https://marketplace.canva.com/MABzgDu4ChI/2/0/thumbnail_large/canva-travel-adventures-youtube-thumbnail-MABzgDu4ChI.jpg"]);
        $videos->save();
        $videos2 = new Video;
        $videos2->course = 1;
        $videos2->order = 2;
        $videos2->status = 1;
        $videos2->name = "Moving forward";
        $videos2->description = "It's getting harder";
        $videos2->endgame = "this time, Lets get your doubts cleared. Ask to me directly ";
        $videos2->files = json_encode(["sources"=>[["src"=>"/videos/sample.mp4","quality"=>"360"]],"thumbnail"=>"https://i.ytimg.com/vi/bTqVqk7FSmY/maxresdefault.jpg"]);
        $videos2->save();
        $videos3 = new Video;
        $videos3->course = 1;
        $videos3->order = 2;
        $videos3->status = 1;
        $videos3->name = "The Next Step";
        $videos3->description = "It's getting harder";
        $videos3->endgame = "this time, Lets get your doubts cleared. Ask to me directly ";
        $videos3->files = json_encode(["sources"=>[["src"=>"/videos/sample1.mp4","quality"=>"360"]],"thumbnail"=>"https://www.vtracrobotics.com/wp-content/uploads/2018/07/video-thumbnail.jpg"]);
        $videos3->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
