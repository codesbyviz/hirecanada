import Axios from "axios";
import { resolve } from "url";
import { reject } from "q";

  const stores = {
    state:{
        app:{
            version:"1.0.0",
            name:"Hire Canada",
            logo:"/images/logo.png",
            razorpay:{
                key:"rzp_test_WhGYHyk32UGPxP",
                secret:"7FdN3Wurqbd0MlRkjQI17rFU",
                apiendpoint:"https://api.razorpay.com/v1"
            }
        },
        axios:{
            
        },
        video:{
            playing:false,
        },
        page:{
            nav:true,
            home:false,
            isscrller:false,
        },
        isSubmitting:false,
        name:"Your Name",
        user:{},
        token:localStorage.getItem('_token') ? localStorage.getItem('_token'):false,
    },
    mutations(){
        
    },
    mutations:{
        SetToken(state,token){
            state.token = token;
        },
        ActivateUser(state,user){
            state.user = user;
        }
    },
    actions:{
        Login(context,credentials){
            return new Promise((resolve,reject)=>{
                Axios.post("/api/login",{
                    username:credentials.username,
                    password:credentials.password,
                }).then((res)=>{
                    localStorage.setItem('_token',res.data.access_token);
                    context.commit('SetToken',res.data.access_token)
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        GetUser(context){
            return new Promise((resolve,reject)=>{
                Axios.get("/api/user",{
                    headers: {
                        "Authorization" : "Bearer "+context.state.token
                    },
                }).then((res)=>{
                    resolve(res);
                    context.commit('ActivateUser',res.data)
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        SignUp(context,credentials){
            return new Promise((resolve,reject)=>{
                Axios.post("/api/signup",{
                    name:credentials.name,
                    phone:credentials.phone,
                    email:credentials.username,
                    password:credentials.password,
                }).then((res)=>{
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        LogOut(context,credentials){
            return new Promise((resolve,reject)=>{
                Axios.get("/api/logout",{
                    headers: {
                        "Authorization" : "Bearer "+credentials.token
                    },
                }).then((res)=>{
                    resolve(res);
                    localStorage.removeItem('_token');
                    context.commit('SetToken',false)
                    context.commit('ActivateUser',{})
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        Courses(context,data){
            return new Promise((resolve,reject)=>{
                Axios.get("/api/courses",{
                    headers: {
                        "Authorization" : "Bearer "+data.token
                    },
                }).then((res)=>{
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        Course(context,data){
            return new Promise((resolve,reject)=>{
                Axios.get("/api/courses/"+data.course,{
                    headers: {
                        "Authorization" : "Bearer "+data.token
                    },
                }).then((res)=>{
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        AdminUser(context,data){
            let instance = Axios.create({baseURL:"/api/",headers: {'Authorization': 'Bearer '+context.state.token}})
            return new Promise((resolve,reject)=>{
                instance.get("user/"+data.id)
                .then((res)=>{
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        UpdateUser(context,data){
            let instance = Axios.create({baseURL:"/api/",headers: {'Authorization': 'Bearer '+context.state.token}})
            return new Promise((resolve,reject)=>{
                instance.put("user/"+data.id,{user:data.data})
                .then((res)=>{
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        CourseCreate(context,data){
            let course = data.course;
            let instance = Axios.create({baseURL:"/api/",headers: {'Authorization': 'Bearer '+context.state.token}})
            return new Promise((resolve,reject)=>{
                instance.post("courses",{course})
                .then((res)=>{
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        CourseUpdate(context,data){
            let course = data.course;
            let instance = Axios.create({baseURL:"/api/",headers: {'Authorization': 'Bearer '+data.token}})
            return new Promise((resolve,reject)=>{
                instance.put("courses/"+course.id,{course})
                .then((res)=>{
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        GetPurchase(context,data){
            let instance = Axios.create({baseURL:"/api/",headers: {'Authorization': 'Bearer '+context.state.token}})
            return new Promise((resolve,reject)=>{
                instance.get("purchases/"+data.id)
                .then((res)=>{
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        CourseAccess(context,data){
            return new Promise((resolve,reject)=>{
                Axios.get("/api/course/"+data.course,{
                    headers: {
                        "Authorization" : "Bearer "+data.token
                    },
                }).then((res)=>{
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        VideoAccess(context,data){
            let instance = Axios.create({headers: {'Authorization': 'Bearer '+context.state.token}})
            return new Promise((resolve,reject)=>{
                instance.put("/api/video/"+data.video,data).then((res)=>{
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        Coupon(context,data){
            return new Promise((resolve,reject)=>{
                Axios.get("/api/coupons/"+data.code,{
                    headers: {
                        "Authorization" : "Bearer "+data.token
                    },
                }).then((res)=>{
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        Coupons(context,data){
            let instance = Axios.create({headers: {'Authorization': 'Bearer '+context.state.token}})
            return new Promise((resolve,reject)=>{
                instance.get("/api/coupons/")
                .then((res)=>{
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        CreateCoupons(context,data){
            let instance = Axios.create({headers: {'Authorization': 'Bearer '+context.state.token}})
            return new Promise((resolve,reject)=>{
                instance.post("/api/coupons",{
                    coupons_number:data.number,
                    type:data.type,
                    value:data.value,
                })
                .then((res)=>{
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        SaveTransaction(context,data){
            let formdata = data.data;
            let instance = Axios.create({headers: {'Authorization': 'Bearer '+data.token}})
            return new Promise((resolve,reject)=>{
                instance.post('/api/purchases',{formdata})
                .then((res)=>{
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        AdminCourses(context){
            let instance = Axios.create();
            return new Promise((resolve,reject)=>{
                instance.get('/api/admin/courses')
                .then((res)=>{
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
        Adminusers(context){
            let instance = Axios.create();
            return new Promise((resolve,reject)=>{
                instance.get('/api/admin/users')
                .then((res)=>{
                    resolve(res);
                }).catch((error)=>{
                    reject(error);          
                })
            }
        )},
    },
};
export default stores;