import Vuex from 'vuex';
import VueRouter from 'vue-router'
import routes from './routes';
import stores from './store';
import VuePlyr from 'vue-plyr';
import CoursesViewLoader from './components/Modules/Loaders/CoursesViewLoader'
import VideoThumbLoader from './components/Modules/Loaders/VideoThumbLoader'
import VideoThumbnail from './components/Modules/Access/VideoThumbnail'
import Video from './components/Modules/Access/Video'
import FooterComponent from './components/Modules/FooterComponent'
import PreLoader from './components/Modules/Loaders/PreLoader'
import Progressiveloader from './components/Modules/Loaders/Progressiveloader'

import { VueEditor } from 'vue2-editor'



import Swiper from 'vue-swiper'
window.cors = require('cors')

require('./bootstrap');
window.Noty = require('noty');
window.NProgress = require('NProgress');
window.Vue = require('vue');
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VuePlyr)
Vue.use(VueEditor)
Vue.use(require('vue-moment'));


// NPrgoress Configs
NProgress.configure({ showSpinner: false });
Noty.overrideDefaults({
    layout   : 'bottomRight',
    theme    : 'nest',
    timeout:4000,
    closeWith: ['click'],
    animation: {
        open : 'animated fadeInUp',
        close: 'animated fadeOutDown'
    }
});

const router = new VueRouter({
    routes,
    mode: 'history',
    linkExactActiveClass: 'active'
})

const store = new Vuex.Store(stores);
router.beforeEach((to, from, next)=>{
    NProgress.start();
    store.state.page.nav = true;
    store.state.page.home = false;
    if (to.matched.some(record => record.meta.Auth)) {
        if(!store.state.token){
            next({path:"/login"});
        }
        else{
            store.dispatch('GetUser',{token:store.state.token})
            // .then(res=>{console.log(res.data)});
            next();
        }
    }
    else if(to.matched.some(record => record.meta.Any)){
        if(store.state.token){store.dispatch('GetUser',{token:store.state.token})}
        next();
    }
    else if(to.matched.some(record => record.meta.Paid)){
        if(store.state.token){
            store.dispatch('GetUser',{token:store.state.token})
            if(store.state.user.amount_paid == null){
                next({path:"/continue-payment"});
            }
            else{
                next();
            }
        }
        else{
            next({path:"/login"});
        }
        // next();
    }
    else if(to.matched.some(record => record.meta.Guest)){
        if(store.state.token){
            next({path:"/dashboard"});
        }
        else{
            next();
        }
    }
    else{
        next()
    }
})
router.afterEach((to, from)=>{
    NProgress.done();
});
Vue.component('main-component', require('./components/App.vue').default);
Vue.component('navigation', require('./components/Modules/Navigation').default);
Vue.component('course-view-loader',CoursesViewLoader,{props: ['items']});
Vue.component('video-preloader',VideoThumbLoader,{props: ['items']});
Vue.component('video-thumbnail',VideoThumbnail,{props: ['video']});
Vue.component('video-player',Video,{props: ['source']});
Vue.component('swiper',Swiper);
Vue.component('footer-component',FooterComponent);
Vue.component('preloader',PreLoader);
Vue.component('progressive-loader',Progressiveloader);
const app = new Vue({
    components: {
        VueEditor
      },
 
    router,
    store
}).$mount('#app');
