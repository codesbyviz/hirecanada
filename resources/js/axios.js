import axios from 'axios';
import stores from './store'
const axiosInstance = axios.create({
  baseURL: '/api/',
  headers: { Authorization: `Bearer `+stores.state.token },
});

export default axiosInstance;