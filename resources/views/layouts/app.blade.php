<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="/js/app.js" defer></script>
    <script src="https://unpkg.com/jquery@3.3.1/dist/jquery.js"></script>
    <script src="https://unpkg.com/wowjs@1.1.3/dist/wow.js"></script>

    <script src="https://checkout.razorpay.com/v1/checkout.js" crossorigin="" onerror="alert(this)"></script>
    <script>new WOW().init();</script>
    <link href="/css/app.css" rel="stylesheet">
</head>

<body>
    <div id="app">
        <main>
            <main-component></main-component>
        </main>
        <footer-component></footer-component>
    </div>
</body>
</html>
